﻿open System

let inline (~~) f a b = f b a
let inline ψ     x    = x |> box |> unbox
let inline Δ<'a>      = Unchecked.defaultof<'a>
let inline show  x    = printfn "%A" x
let inline trace x    = show x; x
let inline pause ()   = Console.ReadLine() |> ignore; 0
let inline init  s    = let c = new ^t() in Seq.iter (fun (k,v) -> (^t : (member    Add : 'a * 'b -> unit) c, k, v)) s; c
let inline init_ s    = let c = new ^t() in Seq.iter (fun (k,v) -> (^t : (member TryAdd : 'a * 'b -> bool) c, k, v) |> ignore) s; c
open System.Text.RegularExpressions
let (|R|_|) regex str = let  m = Regex(regex).Match(str) in if m.Success then Some (List.tail [ for x in m.Groups -> x.Value ]) else None

module Enum = let parse<'T when 'T : (new : unit -> 'T) and 'T :> ValueType and 'T : struct> s = let _, o = Enum.TryParse<'T>(s) in o

module Array =
  let swap i0 i1 (a : 'a []) = let t0, t1 = a.[i0], a.[i1] in a.[i1] <- t0; a.[i0] <- t1
  let share s (a : 'a []) = seq { let o = ref 0 in for c in (s:int seq) do yield a.[!o..(!o)+c-1]; o := !o + c }
  let split s (a : 'a []) = seq { for i = 0 to (int <| ceil (float a.Length/float s))-1 do yield a.[ i*s .. (min ((i+1)*s) a.Length)-1 ] }
let (<~) (a : 'a []) (b : 'a []) = for i = 0 to (min a.Length b.Length) - 1 do a.[i] <- b.[i]
let (!~) f x = Seq.toArray <| f x
let (!+) f x = Seq.toList  <| f x
let inline (+~) a b = Array.append a b



open System.Collections.Generic
type KeyedByTypeCollection<'C> with member inline c.Change<'T> a = let x = c.Remove<'T>() in a x; c.Add(ψ x)



open Microsoft.ServiceModel.WebSockets
open System.ServiceModel.Description

let host<'a> es =
  let s = 1024 * 1024 * 1;
  let h = new  WebSocketHost(typedefof<'a>, es |> Seq.toArray) in h.Faulted.Add <| fun x -> show "faulted"; show x
  let b =      WebSocketHost.CreateWebSocketBinding(false, s, s)
  let e = h.AddWebSocketEndpoint b
  h.Description.Behaviors.Change <| fun (b : ServiceDebugBehavior) -> b.HttpHelpPageEnabled <- false; b.HttpsHelpPageEnabled <- false
  h

let inline (<--) (c : WebSocketService) m = c.Send (m.ToString()) |> ignore



module Persona =

  open System.Net
  open System.Text
  open System.Json
  open System.Collections.Specialized

  let verify assertion audience =
    use w = new WebClient()
    let a = "https://verifier.login.persona.org/verify"
    let c : NameValueCollection = init [ "assertion", assertion; "audience", audience ]
    try w.UploadValues(a, c) |> Encoding.UTF8.GetString |> trace |> JsonValue.Parse with e -> JsonPrimitive e.Message :> JsonValue



open System.Json
type O = JsonObject
let inline (!@) x = JsonValue.Parse x
let inline (!^) x = x :> JsonValue
let inline js   x = !^ JsonPrimitive(x.ToString())
let inline (?)  (j : JsonValue) (k : string) = match j.GetValue k with :? JsonPrimitive as p -> p.Value.ToString() | _ -> null
let inline (?-) (j : JsonValue) (k : string) =       j.GetValue k
let (|C|_|) cmd (json : JsonValue) = try if json.ContainsKey cmd then Some (json ?- cmd) else None with _ -> None



module Identity =
  type User = string
  module Providers =
    let tryLogin s o =
      try let   i = !@s ?- "identity"
          match i ? provider with
          | "persona" -> (Persona.verify (i ? assertion) o) ? email
          | "KMA"     ->                 (i ? level) + "@kma.ac.kr"
          | _ -> Δ
      with  _ -> Δ



module Corsis =

  open Identity
  open System.Collections.Generic
  open System.Collections.Concurrent

  type Store<'K, 'V> = ConcurrentDictionary<'K, 'V>
  type ConcurrentDictionary<'K, 'V> with
    member d.NewOrModify k n f = d.AddOrUpdate(k, (fun _ -> let v = n k in f k v; v), (fun _ v -> f k v; v))
    member d.Try k a = try let f,v = d.TryGetValue k in if f then a v with _ -> ()

  let inline (<->) k v = KeyValuePair(k,!^ v)
  let inline (~-)  x   = ignore x

  type View    = WebSocketService
  type Message = JsonValue
  type JsonValue with static member Silence = Δ


  module Spaces =

    type Name  = String
    type Reach = Space = 0 | User = 1 | View = 2

    type Space<'O, 'V when 'V :> View and 'V : equality>(o : 'O) =
      let clients       = Store<User, 'V list>()
      let dispatch m    = clients.Values |> Seq.iter (Seq.iter (~~ (<--) m))
      let announce u a  = dispatch <| trace (O["Space" <-> O["users" <-> JsonArray(clients.Keys |> Seq.map js); "self" <-> js u; "action" <-> js a]])

      let space  u m = dispatch <| trace (O["user" <-> js u; "data" <-> m])
      let user   u m = clients |> Seq.iter (fun c -> if c.Key = u then c.Value |> Seq.iter (~~ (<--) (trace <| O["user" <-> js u; "data" <-> m])))
      let view v u m = v <-- (trace <| O["user" <-> js u; "data" <-> m])

      member val Object = o : 'O
      member r.Clients  = clients

      member r.send x v u m = if m <> null then match x with Reach.Space -> space u m | Reach.User -> user u m | _ -> view v u m 

      member r.enter  v u = if   u <> null
                            then -clients.AddOrUpdate(u, (fun _ -> [v]), (fun _ vs -> v::vs))
                                 announce u "enter"

      member r.exit   v u = if   u <> null
                            then match clients.[u] |> List.filter ((<>) v) with
                                 | [] -> -clients.TryRemove   u
                                 | vs -> -clients.AddOrUpdate(u, [], fun _ _ -> vs)
                                 announce u "exit"


  module States =

    open Spaces

    type IState =
      abstract member announce :         (View -> User -> Message -> unit) -> unit
      abstract member interact : Reach -> View -> User -> Message -> Message
      abstract member update   :          User -> (Space<IState, 'V :> View>) -> unit
      abstract member drop     :          User -> unit



  module Concordance =

    type Name = String
    type Text = String
    
    type Concordancer() =
      member val Files  = Store<Name, Text>()
      member c.Search q =
        try   let r = Regex(q, RegexOptions.Compiled ||| RegexOptions.CultureInvariant ||| RegexOptions.IgnoreCase, TimeSpan.FromSeconds 3.)
              let lr = 50
              let rr = 50
              let S  = 0
              JsonArray(seq {
              for f in c.Files do
                let n  = f.Key
                let t  = f.Value
                let L  = t.Length
                for m in r.Matches t do
                  let cs = m.Index
                  let cl = m.Length
                  let ce = cs + cl
                  let ls = max S (cs-lr)
                  let le = cs
                  let rs = ce
                  let re = min L (rs+rr)
                  yield O["File" <-> js n; "Left" <-> js (t.Substring(ls, le-ls)); "Center" <-> js m.Value; "Right" <-> js (t.Substring(rs, re-rs))] :> JsonValue
              })
        with _ -> JsonArray()

    open States
    open Spaces

    open System.IO

    type Web(c : Concordancer) =

      let fs = c.Files
      do try Directory.EnumerateFiles(Environment.CurrentDirectory, "*.txt") |> Seq.iter (fun f -> try ignore <| fs.TryAdd(Path.GetFileName f, File.ReadAllText f) with _ -> ()) with _ -> ()

      static member files    (c : Concordancer) = O["Files" <-> JsonArray(c.Files |> Seq.map (fun f -> O["Name" <-> js f.Key; "Size" <-> JsonPrimitive(f.Value.Length)] :> JsonValue))]
      static member search q (c : Concordancer) =
        let ms = c.Search q
        O["Concordance" <-> O["Query" <-> js q; "Matches" <-> ms]]

      interface IState with
        member o.update   _ _ = ()
        member o.drop     _   = ()
        member o.announce   a = a Δ "login" <| Web.files c
        member o.interact r u v m = // u is admin
          match r with
          | Reach.Space ->
            match m with
            | C "Add"    (C "File"  f) -> show "AF"; let n = f ? Name in let   t = f ? Text in fs.AddOrUpdate(n, t, fun _ _ ->   t) |> ignore; c |> Web.files
            | C "Add"    (C "Slice" s) -> show "AS"; let n = s ? Name in let   t = s ? Text in fs.AddOrUpdate(n, t, fun _ v -> v+t) |> ignore; c |> Web.files
            | C "Remove" (C "File"  f) -> show "RF"; let n = f ? Name in let r,t =             fs.TryRemove   n in (if r then (c |> Web.files) else Δ)
            | _ -> show "?"; show (m.ToString()); Message.Silence
            :> Message
          | Reach.View ->
            match m with
            | C "Query" q -> show "QC"; c |> Web.search (q.ReadAs())
            | _ -> show "?"; show (m.ToString()); Message.Silence
            :> Message
          | _ -> Message.Silence :> Message



  module Services =    

    open Concordance
    open Spaces
    open States

    let private spaceState n = match n with
                               | R @".*?(\w+).*" [ g ] when g = "ca" -> Web(Concordancer())
                               | _                                   -> raise <| InvalidOperationException()
                               :> States.IState

    open System.Web
    open System.Collections.Specialized
    type DynamicUserStateSpace() =
      inherit WebSocketService()
      let mutable    u = null : User
      let mutable    q = null : NameValueCollection
      static let     s = Store<Name, Space<IState, View>>()
      member v.login x = u <- Providers.tryLogin x v.WebSocketContext.Origin
                         q <- trace <| HttpUtility.ParseQueryString <| trace v.WebSocketContext.RequestUri.Query
                         show (q.Get "a" |> s.ContainsKey)
                         let w = (s.NewOrModify  q.["a"] (fun _ -> show (0, q.["a"]); Space(spaceState q.["s"])) <| fun _ s -> (show (1, q.["a"]); s.enter v u))
                         w.Object.announce <| s.[q.["a"]].send Reach.Space
      override v.OnMessage r = match u with
                               | null -> v.login r; (if u = null then v.Close())
                               | _    -> let m = !@r in let s = s.[q.["a"]]
                                         s.Object.interact Reach.Space v u m |> s.send Reach.Space v u
                                         s.Object.interact Reach.User  v u m |> s.send Reach.User  v u
                                         s.Object.interact Reach.View  v u m |> s.send Reach.View  v u
                                         s.Object.update   u s

      override v.OnClose() =
        try       s.[q.["a"]].Object.drop u
                  s.Try q.["a"] <| fun r -> (r.exit v u; if r.Clients.Count = 0 then let _,_ = s.TryRemove q.["a"] in ())
        with _ -> ()



[<EntryPoint>]
let main _ = let ss = host<Corsis.Services.DynamicUserStateSpace> [ Uri "ws://localhost:4000" ] in ss.Open(); pause()