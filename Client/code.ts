/// <reference path="c:\corsis\kma\jquery.d.ts" />

// Copyright � 2012 Cetin Sert

interface Navigator { id: any; }
declare var MD5: any;

function tab(url) { window.open(url, '_blank'); window.focus(); }
function qs(name : string)
{
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if(results == null) return "";
  else return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function visitGravatar() { tab("https://en.gravatar.com"); }
function gsrc(email) { return '//en.gravatar.com/avatar/' + MD5(email); }
function gravatar(email) { return $('<img>').attr({ class: "gravatar", src: gsrc(email) }).click(visitGravatar); }

function request() { navigator.id.request({ siteName: "Korea Military Academy" }); e.hide(); }

var ss = ["ws://localhost:4000?a=x&s=con"];
var cs = ss[0];


function connect(act) {
    s = new WebSocket(cs);
    s.onmessage = function(m) { var x = m.data; process(JSON.parse(x)); };
    s.onclose   = function()  { err(cs, "closed"); };
    s.onopen    = function()  { act(); watch(); };
}
var s : any = { readyState: 0 };
function reconnect() { if (s.readyState != WebSocket.OPEN) connect(function() { if (as != null) s.send(as); e.hide(); }); }
connect(function(){});

var as = null;
var si = $('#si'   ).click(function() { connect(function() { e.hide(); }); request(); });
var so = $('#so'   ).click(function() { as = null; navigator.id.logout(); s.close(); sl.empty(); });
var e  = $('#error').click(function() { $(this).hide(); });
var l  = $('#list');
var sl = $('#self'); 
var me = $('#me').click(visitGravatar);
//var fu = $('#fu');

function lin() { si.hide(); so.show(); }
function lout() { so.hide(); si.show(); }

function watch() {
    navigator.id.watch({
        onlogin:  function(a) { as = { identity: { provider: "persona", assertion: a } }; s.send(JSON.stringify(as)); },
        onlogout: function()  { lout(); }
    });
}

function process(m) { if (m.Space) list(m.Space); }
function err(s, m) { $('strong').text(s); $('#msg').text(m); $('#error').show(); lout(); }

function list(r) {
    lin(); l.empty();
    for (var i=0; i<r.users.length; i++) {
        var e = r.users[i];
        l.append($('<li/>').append(gravatar(e)).append($('<div/>', { class: "user", text: e })));
    }
    if (sl.text() == "") { sl.text(r.self); me.attr('src', gsrc(r.self)); }
}

function fus() { alert(document.getElementById("fu").files[0]); }