﻿// Copyright © 2013 Cetin Sert

function tab(url) { window.open(url, '_blank'); window.focus(); }
function qs(name) {
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if (results == null) return "";
  else return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function visitGravatar() { tab("https://en.gravatar.com"); }
function  gsrc(email) { return '//en.gravatar.com/avatar/' + MD5(email); }
function kgsrc(email) { if (email == "student@kma.ac.kr") return "http://upload.wikimedia.org/wikipedia/en/9/99/Korea_Military_Academy_Emblem.jpg"; else return gsrc(email); }
function gravatar(email) { return $('<img>').attr({ class: "gravatar", src: kgsrc(email) }).click(visitGravatar); }


///
var space  = qs("space") || "KMA1";
var state  = qs("state") || "ca";
var server = ["ws://"+window.location.hostname+":4000?a="+space+"&s="+state]

function KMA()          { console.warn("KMA-login");         sendJSON({ identity: { provider: "KMA",     level: "student" } }); }
function personaWatch() { console.warn("persona-watch");
  navigator.id.watch({
    onlogin:  function (a) { console.warn("persona-login");  sendJSON({ identity: { provider: "persona", assertion: a } }); },
    onlogout: function ()  { console.warn("persona-logout"); }
  });
}
function personaRequest() { navigator.id.request({ siteName: "Korea Military Academy" }); }

var s = { readyState: 0 };
function personaLogin() { personaWatch(); personaRequest(); }
function     KMALogin() { KMA(); }
function connect(login) {
  console.warn("connect: " + server);
  l.empty(); if (s.close) s.close();
  s = new WebSocket(server);
  s.onmessage = function(m) { var x = m.data; process(JSON.parse(x)); };
  s.onclose   = function()  { err(server, "closed"); };
  s.onopen    = function()  { error.hide(); levelButton.hide(); login() };
}

var teacher = $('#teacher').click(function () { connect(personaLogin); });
var student = $('#student').click(function () { connect(    KMALogin); });
var leave   = $('#leave'  ).click(function () { s.close(); });
var error   = $('#error'  ).click(function () { $(this).hide(); });
var l       = $('#list');


teacher.hide(); student.hide(); leave.hide();
var levelButton = qs("level") == "teacher" ? teacher : student; levelButton.click();
/*HACK*/ if (qs("level") != "teacher") { $("<style type='text/css'> .admin { display: none; } #fileZone { border: none; border-bottom: 1px solid gray; } </style>").appendTo("head"); }


var lpm = null; // for browser console debugging
function process(m) { if (m.Space) list(m.Space); else { console.warn(JSON.stringify(lpm = m)); pud(m.data); } }
function err(s, m) { $('#strong').text(s); $('#msg').text(m); $('#error').show(); levelButton.show(); }

function list(r) { l.empty(); for (var i = 0; i < r.users.length; i++) { var e = r.users[i]; l.append($('<li/>').append(gravatar(e)).append($('<div/>', { class: "user", text: e }))); } }

////////

function pud(d) {
  if (d.Files)       pumFiles(d.Files);
  if (d.Concordance) pumConcs(d.Concordance);
}

function sendJSON(j) { s.send(JSON.stringify(j)); }
function removeFile(f) { sendJSON({ Remove: { File: { Name: f } } }); }
function fileEntry(o, f) { o.push('<li><a class="admin" onclick="removeFile(\'', escape(f.Name), '\')">-</a><strong>', escape(f.Name), '</strong> ', f.Size ,'</li>'); }
function pumFiles(fs) { var o = []; for (var i = 0, f; f = fs[i];         i++) fileEntry(o, f); document.getElementById('fileList').innerHTML = '<ul>' + o.join('') + '</ul>'; }
function pumConcs(co) { var o = []; for (var i = 0, m; m = co.Matches[i]; i++) matchEntry(o, m); document.getElementById('concordance').innerHTML = "<div>"+co.Matches.length+" result(s)</div>" + o.join(''); }
function flatten(s) { return s.replace(/[\r\n]/g, ''); }
function matchEntry(o, m) {
  o.push('<tr><td class="source">', escape(m.File), '</td><td class="left"><pre>', flatten(m.Left), '</pre></td><td class="center"><pre>', flatten(m.Center), '</pre></td><td class="right"><pre>', flatten(m.Right), '</pre></td></tr>');
}



// upload

function uploadSliced(f, chunk) {
  var s = 0; var c = Math.min(chunk, f.size); var e = c;
  var r = new FileReader(); console.warn(f.size);

  r.onloadend = function (evt) {
    if (evt.target.readyState == FileReader.DONE) {
      var o = { Add: { Slice: { Name: f.name, Offset: s, Size: f.size, Text: evt.target.result } } };
      sendJSON(o); // console.warn(o);
      var blob = f.slice(s += c, e += c);
      if (s < f.size) r.readAsBinaryString(blob);
    }
  }

  var blob = f.slice(s, e);
  r.readAsBinaryString(blob);  
}

function uploadWhole(f) {
  var r = new FileReader();
  r.onload = (function (f) { return function(e) { sendJSON({ Add: { File: { Name: f.name, Text: e.target.result } } }); } }) (f);
  r.readAsBinaryString(f);
}

function handleFileSelect(evt) { evt.stopPropagation(); evt.preventDefault();
  var files = evt.dataTransfer.files;
  var o = []; for (var i = 0, f; f = files[i]; i++) { //o.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ', f.size, ' bytes, last modified: ', f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString(): 'n/a', '</li>');
    //uploadWhole(f);
    uploadSliced(f, 1024*10);
  } //document.getElementById('fileList').innerHTML = '<ul>' + output.join('') + '</ul>';
}

function handleDragOver(evt) { evt.stopPropagation(); evt.preventDefault(); evt.dataTransfer.dropEffect = 'copy'; }

var fileZone = document.getElementById('fileZone');
fileZone.addEventListener('dragover', handleDragOver, false);
fileZone.addEventListener('drop', handleFileSelect, false);

// upload



// query

var q = $('#query').keydown(function(e) { if (!e.shiftKey && e.which == 13) sendQuery(); });

function sendQuery() { sendJSON({ Query: "\\b" + q.val().trim() + "\\b"}); }